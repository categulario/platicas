class: center, middle, dark

# El caso contra github

<small>por @Categulario<br />para @codeandomexico</small>

TLDR; Github no es necesario, y quedarse es dañino.

---
class: middle, dark

## Áreas de reflexión

* Tecnológica
* Social
* Política
* Opciones
* Problemas con estas ideas

---

## Aspecto tecnológico

**Github no es necesario**

* github no provee de alguna característica indispensable que no sea
  reemplazable, pero sí se queda corto en algunas como:

  * CI/CD (gitlab CI es más flexible y ha tenido más tiempo para madurar).
  * Manejo de paquetes y contenedores (el registro de gitlab cuenta con
    namespaces).

* Existen afortunadamente muchísimas altenativas según el nivel de compromiso
  político que tenga uno con la causa del software libre/código
  abierto/autonomía tecnológica.

---

## Aspecto político

**Github es dañino**

* github es una plataforma abusiva, y esto es cierto aun antes de microsoft. Se
  permiten ajustar las políticas de uso de servicio de la noche a la mañana
  notificando en un correo largo y confuso cómo ahora pueden explotar tu código
  públicamente hospedado para preparar un servicio que luego te van a vender.

* El respeto a la privacidad no es una característica y mucho menos una
  prioridad de la plataforma.

* sus valores no coinciden con la posición que pretenden ocupar en la comunidad.
  La meca del código abierto debe ser de código abierto también.

* Mantienen relaciones estrechas con actores que cometen otro tipo de abusos y
  represiones inaceptables (véase [github drop
  ICE](https://github.com/selfagency/microsoft-drop-ice)).

---

## Aspecto social

**Su consumo es dañino a gran escala**

* Su uso perpetúa su dominio. Pequeños actores (individuos) pueden tomar una
  posición al respecto dejando el servicio, pero es hasta que actores más
  grandes (como codeandoméxico) toman acciones al respecto que una mayor porción
  de la comunidad puede entrever que existe un problema.

* La existencia de un proveedor "default" (hegemonía) de cierto servicio
  dificulta (cuando no aniquila) la diversidad de opciones y puntos de vista. En
  este punto hay personas que creen que `control de versiones = git = github`.

---

## Opciones

Siendo realistas existen estas opciones:

* Gitlab.com

    * el más sencillo
    * cuenta con herramienta de migración
    * auto-hospedable
    * muy buenas características

* Codeberg.org

    * sin ánimo de lucro
    * un muy completo conjunto de valores
    * sería bueno hacerles una donación
    * usa forgejo (software libre)

* hospedar un fogejio

    * es necesario hospedar, pero no consume muchos recursos (y tenemos un
      servidor para eso).
    * requiere administración

---

## Principales problemas con estos argumentos

* Para que realmente funcionen deben ser asimilados por el equipo. En este
  momento son míos y si no estoy no hay quien los defienda. Es difícil sostener
  una decisión así.

* Esta decisión es por definición ir contracorriente. Muchas si no todas las
  personas que consideran contribuir con codeandoméxico caen en el segundo punto
  de la sección [Aspecto Social](#5), lo cual podría constituir una razón de
  peso para no tomar ninguna acción al respecto, sin embargo también es notable
  que las personas que contribuyen (revisé los repositorios fijados) son un
  número reducido que seguramente tendrá el entendimiento de seguir sus
  contribuciones donde estas estén.

* Es una decisión incómoda de tomar, al final se tiene ya un servicio confiable
  que funciona. Moverse implica un esfuerzo adicional.

---
class: middle, center, dark

# Gracias
