class: center, middle, dark

# Pizarra: logros, retos y futuro

<small>por @Categulario</small>

![ícono de la pizarra](assets/img/pizarra.png)

---
class: middle

## Yo solo quería dibujar

* vectores,
* en un espacio infinito,
* con zoom igual de infinito,
* usando mi nueva tableta gráfica,
* y estrenar lenguaje de programación

---
class: center, middle

## Seguro sería sencillo 😅

---
class: center, middle

## Al principio de los tiempos, no había gtk...

---
class: center, middle, dark

<video style="width: 100%" src="assets/video/pizarra_old.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: middle

## Pero esto no era bueno

* Menús y otras cosas no existían
* No había barra de título
* El renderizado de las líneas era horrible
* Las herramientas solo eran accesibles conociendo los misteriosos atajos de
  teclado

---
class: middle

## Entonces llegó GTK

--

### Y la cosa se puso seria

---
class: center, middle, dark

### Aprendí cairo y mejoré mis líneas

<video style="width: 100%" src="assets/video/bug_render_punto_fuga.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### ... luego aprendí a hacerlo bien

<video style="width: 100%" src="assets/video/bug_render_linea_espuria.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### o casi bien ...

<video style="width: 100%" src="assets/video/bug_render_lineas_multiplicadas.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Implementé rectángulos

<video style="width: 100%" src="assets/video/bug_rectangulos.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Aprendí sobre traslaciones

<video style="width: 100%" src="assets/video/bug_render_lineas.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Implementé polígonos y curvas de bezier

<video style="width: 100%" src="assets/video/bug_bezier.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Implementé elipses!

<video style="width: 100%" src="assets/video/bug_elipses_no_verticales.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Aunque no se portaban muy bien que digamos

<video style="width: 100%" src="assets/video/bug_elipses_se_redimensionan.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### O de plano no cooperaban

<video style="width: 100%" src="assets/video/bug_elipses_no_se_mueven.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Total que mejor cambié cómo funcionaban

<video style="width: 100%" src="assets/video/bug_elipse_explota.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Y ya quedaron mejor

<video style="width: 100%" src="assets/video/bug_elipses_con_zoom.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Las rotaciones, como la piedra en el camino

<video style="width: 100%" src="assets/video/bug_rotacion_explota.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### me enseñaron a rodar y rodar

<video style="width: 100%" src="assets/video/bug_rotacion_una_direccion.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Hasta implementé cuadrículas

<video style="width: 100%" src="assets/video/bug_cuadricula_arco.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### No sin unos pocos defectirijillos

<video style="width: 100%" src="assets/video/bug_cuadricula_arte.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Nada que no se pudiera arreglar

<video style="width: 100%" src="assets/video/bug_cuadricula_origen.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Con un poco de amor y código

<video style="width: 100%" src="assets/video/bug_cuadricula_batman.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle, dark

### Y claro, había que hacerlas funcionar con las rotaciones

<video style="width: 100%" src="assets/video/bug_cuadricula_rotacion.mp4" autoplay="" loop="" muted="" playsinline=""></video>

---
class: center, middle

## Y todo eso dio sus frutos

--

hora del demo

---
class: middle

## Futuro

* gtk4/libadwaita/nueva interfaz
* flatpak
* internacionalización
* incrustar imágenes
* insertar texto
* más herramientas (como el compás)
* transiciones suaves en el zoom
* cuadros (como diapositivas) y navegación interna (se acuerdan de prezi/sozi?)

---
class: center, middle

# Gracias!

https://gitlab.com/categulario/pizarra-gtk

<video src="https://i.giphy.com/media/G5h04AkAvAHcs/giphy.mp4" autoplay="" loop="" muted="" playsinline=""><img src="https://i.giphy.com/G5h04AkAvAHcs.gif " alt=""></video>
