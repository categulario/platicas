class: center, middle

# ✨ CSVSC ✨

## Tu framework de procesamiento de datos tabulados de confianza.

por `@categulario`

---
class: center, middle

## Érase una vez, un problema llamado CSVs

![problema](problema.svg)

---
class: center, middle

## ¿Me dedico dos ⏰ a resolver mi problema o tres semanas a escribir una 📚?

---
class: center, middle

## 😈

---
class: center, middle

## Y entonces dijo, "sea python"

## 🐍

---
class: center, middle

## Pero vió que lo hecho _no era bueno_

## 😭

* Hacer refactor daba miedo
* Ninguna promesa de desempeño
* Quizá sea tiempo de probar algo nuevo

---
class: center, middle

## ¿Será que lo pueda reescribir a 🦀 en cuatro horas?

## 🕚 😟

---
class: center, middle

## Spoiler: no, no se puede

# 🚫

---
class: middle

## Pero ¿Cómo se ve? 👀

```rust
    let mut chain = InputStream::from_paths(
            vec![
                /* archivos de entrada */
            ],
            UTF_8,
        )

        .filter_col(/* ... */)

        .add( /* ... */)

        .group(/* ... */)

        .flush( /* ... */)

        .add( /* ... */)

        .del(/* ... */)

        .flush( /* ... */)

        .into_iter();
```

---
class: center, middle

## Aprendizajes 🧙🏽‍♂️

Principalmente tres cosas

---
class: middle

## Jaripeo de tipos 🐂

```rust
pub struct Group<I, F, G> {
    iter: I,
    f: F,
    headers: Headers,
    old_headers: Headers,
    group_by: G,
}

impl<I, F, R, G> IntoIterator for Group<I, F, G>
where
    I: RowStream,
    F: Fn(MockStream<vec::IntoIter<RowResult>>) -> R,
    R: RowStream,
    G: GroupCriteria,
{
}
```

---
class: middle

## Traits por todos lados (y algunos trait objects)

```rust
pub use avg::Avg;
pub use count::Count;
pub use last::Last;
pub use max::Max;
pub use min::Min;
pub use sum::Sum;
pub use mul::Mul;
pub use closure::Closure;

/// Aggregates used while reducing must implement this trait.
pub trait Aggregate {
    /// Updates the current value with the next row of data.
    fn update(&mut self, headers: &Headers, row: &Row) -> error::Result<()>;

    /// Gets the current value.
    fn value(&self) -> String;

    /// Gets this aggregate's colname
    fn colname(&self) -> &str;
}
```

---
class: middle

## Diseño de APIs ✍🏽

* Escribe código que use la api que aun no existe
* Haz feliz al compilador, aunque tengas que hacer trampa
* Haz que funcione, sin hacer trampa

---
class: center, middle

## Quizá la procrastinación no está _tan_ mal.

# 🤡

---
class: middle

## Agradecimientos 🙇🏽‍♂️

A Federico por sus aportes (manejo de errores)

Al primer usuario que no fui yo

Al proyecto que dio la necesidad en primer lugar

A 🦀 por ser un lenguaje tan 🐶

A ustedes por aguantar la plática

---
class: center, middle

## ¡Gracias!

# 🙏🏽

https://docs.rs/csvsc
