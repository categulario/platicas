class: center, middle, dark

# Casos prácticos del manejo de errores en [#rustlang](https://rust-lang.org)

<small>por @Categulario</small>

---
class: center, middle, dark

## Objetivo

llevar un manejo limpio y práctico de los distintos tipos de errores que suceden
en programa de interfaz de línea de comandos.

---
class: center, middle, dark

## Caso de estudio

Una herramienta para llevar registro del tiempo ocupado en diversas actividades:

[tiempo](https://gitlab.com/categulario/tiempo)

---
class: middle, dark

## Consideraciones

* Todos los errores de usuario deben ser reportados por stderr de forma clara.
* Los errores en la aplicación deben dar a entender que algo falló, que no es
  culpa del usuario, y dar la información suficiente para depurarlos.
* si algo falla devolver un código de error distinto de 0.

---
class: center, middle, dark

<video src="https://i.giphy.com/media/kQOxxwjjuTB7O/giphy.mp4" autoplay="" loop="" muted="" playsinline=""><img src="https://i.giphy.com/kQOxxwjjuTB7O.gif " alt=""></video>

---
class: middle, dark

## Tipos Error y Result

Error implementa Display y Debug

```rust
use std::result;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    // Cada posible causa de un fallo está aquí.
}

pub type Result<T> = result::Result<T, Error>;
```

---
class: middle, dark

## Origen de los errores

```rust
use crate::error::{Error, Result};

pub fn parse_time(input: &str) -> Result<DateTime<Utc>> {
    // Aquí se trata con muchísima enjundia de entender lo que el usuario quiso
    // decir con 'when I woke up'

    // Si definitivamente es imposible, se notifica al usuario
    Err(Error::DateTimeParseError(input.into()))
}
```

---
class: middle, dark

## Tránsito de los errores

En cualquier otra parte del código se puede usar ? para subir el error

```rust
use crate::error::Result;
use crate::timeparse::parse_time;

fn some_function(input: &str) -> Result<()> {
    let time = parse_time(input)?; // <-- el error siempre sube

    // haz algo con time

    Ok(()) // esta función terminó bien
}
```

---
class: dark

## Destino de los errores

**Error trap**. Todos los errores suben hasta un paso antes del main, donde se
maneja su salida. Main no devuelve `Result<(), E>` sino `()`. Se usa Display
para mostrarlos a la usuaria.


```rust
use tiempo::error::Result;

fn error_trap(matches: ArgMatches) -> Result<()> {
    // desde aquí hacia adentro se usa ? para subir los errores.
    // Aquí también selecciono el subcomando a ejecutar usando
    // matches.subcommand()
}

fn main() {
    // Por aquí defino la interfaz cli usando Clap
    let matches = App::new("tiempo")
        // subcomandos y etcétera...

    if let Err(e) = error_trap(matches) {
        eprintln!("{}", e);
        exit(1);
    }
}
```

---
class: center, middle, dark

## Y ahora, un poco de magia

<video src="https://i.giphy.com/media/IspgDOjRHwtbi/giphy.mp4" autoplay="" loop="" muted="" playsinline=""><img src="https://i.giphy.com/IspgDOjRHwtbi.gif " alt=""></video>

Y cangrejos

---
class: middle, dark

## Match

invaluable para separar casos

```rust
match try_date {
    LocalResult::None => Err(Error::NoneLocalTime(input.into())),

    LocalResult::Single(t) => Ok(t.with_timezone(&Utc)),

    LocalResult::Ambiguous(t1, t2) => Err(Error::AmbiguousLocalTime {
        orig: input.into(),
        t1: t1.naive_utc(),
        t2: t2.naive_utc(),
    }),
}
```

---
class: middle, dark

## if let

porque no siempre importan todos los casos

```rust
if let Ok(value) = env::var("TIMETRAP_CONFIG_FILE") {
    if value.ends_with(".toml") {
        return Ok(Self::read_from_toml(value)?);
    } else {
        return Ok(Self::read_from_yaml(value)?);
    }
}
```

---
class: middle, dark

## thiserror

No es necesario, pero cómo ayuda

```rust
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("The subcommand '{0}' is not implemented")]
    UnimplementedCommand(String),
}
```

---
class: middle, dark

## unwrap

Créeme, sé lo que hago

```rust
if let Some(m) = caps.name("num") {
    m.as_str().parse().unwrap()
}
```

---
class: middle, dark

## unwrap_or

No todos los errores importan, a veces hay un default.

```rust
let start = args.at.unwrap_or(Utc::now());
```

---
class: middle, dark

## map

no es necesario saber qué hay dentro de un Option o Result para procesarlo ;)

```rust
fn current_sheet(&self) -> Result<Option<String>> {
    let results = self.meta_query(
        "select * from meta where key='current_sheet'",
        &[]
    )?;

    Ok(results.into_iter().next().map(|m| m.value))
}
```

---
class: middle, dark

## map_err

toma un error ajeno, hazlo tuyo 🦝

```rust
fn read_from_yaml(path: &str) -> Result<Config> {
    let path: PathBuf = path.into();

    let mut contents = String::new();
    let mut file = File::open(&path).map_err(|_| FileNotFound(path.clone()))?;

    file.read_to_string(&mut contents).map_err(|_| CouldntRead(path.clone()))?;

    serde_yaml::from_str(&contents).map_err(|error| YamlError {
        path, error
    })
}
```

---
class: middle, dark

## transpose

Compra un `Option<Result<T, E>>` y llévate un `Result<Option<T>, E>`
completamente gratis.

```rust
use crate::error::Result;

fn parse_time(s: &str) -> Result<DateTime<Utc>> { /* código */ }

fn some_function() -> Result<()> {
    let start: Option<DateTime<Utc>> = matches
        .value_of("start")
        .map(|s| parse_time(s))
        .transpose()?;

    Ok(())
}
```

---
class: middle, dark

## `collect::<Result<Vec<T>, E>>()`

No sabía que se podía, escribí el código y funcionó

```rust
fn local_to_utc_vec(entries: Vec<Entry>) -> Result<Vec<Entry>> {
    entries
        .into_iter()
        .map(|e| {
            Ok(Entry {
                start: local_to_utc(e.start)?,
                end: e.end.map(|t| local_to_utc(t)).transpose()?,
                ..e
            })
        })
        .collect()
}
```

---
class: center, middle, dark

## Todo bien y bonito

---
class: center, middle, dark

<video src="https://i.giphy.com/media/fG4KWSrgxT8bLMbTBS/giphy.mp4" autoplay="" loop="" muted="" playsinline=""><img src="https://i.giphy.com/fG4KWSrgxT8bLMbTBS.gif " alt=""></video>

---
class: middle, dark

## El problema con From

```rust
use std::io;

impl From<io::Error> for Error { }
```

---
class: middle, dark

## El problema con From

```rust
use std::io;

impl From<io::Error> for Error { }
```

---
class: middle, dark

## ¿lo ves?

```rust
fn read_config(path: &str) -> Result<()> {
    let mut f = File::open(path)?;
}
```

---
class: center, middle, dark

# )?;

---
class: middle, dark

## Falta contexto!

```rust
use crate::error::{Error::*, Result};

fn read_from_toml<P: AsRef<Path>>(path: P) -> Result<Config> {
    let path: PathBuf = path.as_ref().into();

    let mut contents = String::new();
    let mut file = File::open(&path).map_err(|e| CouldntReadConfigFile {
        path: path.clone(),
        error: e,
    })?;

    file.read_to_string(&mut contents).map_err(|e| CouldntReadConfigFile {
        path: path.clone(),
        error: e,
    })?;

    toml::from_str(&contents).map_err(|error| TomlError {
        path, error
    })
}
```

---
class: middle, dark

## Algunas diferencias con otro tipo de aplicaciones

* En CLI todos los errores llegan inmediatamente al usuario, mejor que sean
  amables
* Todo es texto, eso es fácil
* hay un canal para errores! (stderr)

---
class: center, middle, dark

# Gracias!

https://gitlab.com/categulario/tiempo

<video src="https://i.giphy.com/media/G5h04AkAvAHcs/giphy.mp4" autoplay="" loop="" muted="" playsinline=""><img src="https://i.giphy.com/G5h04AkAvAHcs.gif " alt=""></video>
